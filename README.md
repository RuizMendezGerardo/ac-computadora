```mermaid
graph TB
  subgraph "ㅤ"
  Titulo[Von-Neuman vs Hardvard] --> neu[Von-Neuman]
  Titulo --> hardvard[Hardvard]
  neu --> bio[Fue un científico hungaro que<br>hizo aportaciones a diferentes areas]
  neu --> modelo[Modelo]
  modelo --> pro[Procesador]
  modelo --> alm[Almacenamiento]
  modelo --> per[Periféricos]
  modelo --> bus[Buses]

  pro --> pro1[Recibe información,<br>hace operaciones y<br>devuelve datos]
  alm --> alm1[Guarda información para ser<br>usada después]
  per --> per1[Son dispositivos para<br>comunicar al usuario<br>y a la computadora]
  bus --> bus1[Líneas que interconectan<br>los otros dispositivos]

  hardvard --> def[Es un complemento a la<br>arquitectura de Neuman]
  hardvard --> har1[En la arquitectura de<br>Neuman, al solo tener un<br>bus ocacionaba un<br>cuello de botella]
  har1 --> har2[Para esto, el modelo<br>Hardvard añade más buses]
  har2 --> har3[Separa la memoria en dos:]
  har3 --> mem1[Memoria ROM]
  har3 --> mem2[Memoria RAM]
  mem1 --> rom[Almacena instrucciones]
  mem2 --> ram[Almacena datos]
  
end
```
```mermaid
graph TB
  subgraph "ㅤ"
  Titulo[Super computadoras en México] --> ibm[IBM-650]
  Titulo --> cray[CRAY-402]
  Titulo --> kam[KamBalam]
  Titulo --> mit[Miztli]
  Titulo --> xi[Xiuhcoatl] 
  Titulo --> buap[BUAP]

  ibm --> ibm1[Primera gran computadora de latinoamérica]
  ibm --> ibm2[Usaba tarjetas perforadas]
  ibm --> ibm3[Memoria de 2 Ks]
  ibm --> ibm4[1000 operaciones por segundo]

  cray --> cray1[Primera super computadora de latinoamérica]
  cray --> cray2[Al servicio durante 10 años]
  cray --> cray3[Equivalente a 2000 computadoras]

  kam --> kam1[Uso para proyectos de investigación]
  kam --> kam2[Reducia el trabajo de 25 años a una semana]
  kam --> kam3[Equivalente a 1300 computadoras]

  xi --> xi1[Inicio su servicio en 2013]
  xi --> xi2[Amplió su capacidad a 8344 procesadores]
  xi --> xi3[Rendimiento de 252 teraflops]

  buap --> buap1[Una de las computadoras más poderosas de latam]
  buap --> buap2[Almacenamiento equivalente a 5000 computadoras portátiles]
  buap --> buap3[2000 millones de operaciones por segundo]
  buap --> buap4[Se puede usar remotamente]
  
end
```

(No encontré como hacer que se vea menos aplastado :c)